## Drum Sequencer for MaxMSP

Drun Sequencer for MaxMSP with support for sample drag and drop, bpm sync and audo manipulation.

![UI Example](https://i.imgur.com/i6M8EcU.png "UI Example")
